package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    

    private int maxCapacity;
    private ArrayList<FridgeItem> items;

    public Fridge(ArrayList<FridgeItem> items, int maxCapacity){
        this.items=items;
        this.maxCapacity=maxCapacity;
    }
    @Override
    public int nItemsInFridge() {
        return items.size();
    }
    @Override
    public int totalSize() {
        return maxCapacity;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
  
        if (items.size()<20){
            items.add(item); 
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {

        
        if (!items.contains(item)){
        throw new NoSuchElementException();
        }
        else{
            items.remove(item);
        }
    }

    @Override
    public void emptyFridge() {
        items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems=new ArrayList<FridgeItem>();
        for (int i=0;i<items.size();i++){

            if (items.get(i).hasExpired()){
                expiredItems.add(items.get(i));
            }

        
        }
        for (int j=0;j<expiredItems.size();j++){
            items.remove(expiredItems.get(j));
        }
        return expiredItems;
    }
}
